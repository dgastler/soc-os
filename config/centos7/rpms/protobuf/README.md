#The build_rpms.sh will build the protobuf rpms from the source rpm

##build the rpms from the src rpm
To build this for a specific OS/CPU, use docker with the osbuilder containers from dgastler

Example for centos7 arm32:
docker run --rm -i -t -v ${PWD}:/app dgastler/osbuilder-centos7:1.0.0-arm32v7 /bin/bash /app/build_rpms.sh

Example for centos7 arm64:
docker run --rm -i -t -v ${PWD}:/app dgastler/osbuilder-centos7:1.0.1-arm64v8 /bin/bash /app/build_rpms.sh


##Hacking the src RPM
The protobuf cento7 source rpms specifically don't build for python3 and need to be fixed.
They also install java bindings that are not needed and cause depenency issues on the centos7 builds
To fix these issues, the source rpm need to be extracted, modified, and repackaged.

'''rpm -i protobuf-3.6.1-4.el7.src.rpm'''

This command will build the rpm structure in ~/rpmbuild and the modifications needed will all be in "~/rpmbuild/SPECS/protobuf.spec"
The primary change is to change line 3 "%if 0%{?fedora} || 0%{?rhel} > 7" to "%if 0%{?fedora} || 0%{?rhel} > 6" to trick it to treating centos7 like centos8.
Every reference to java was also removed from the spec file.

Once that is done, rebuild the source rpm with the following command (tell rpmbuild to use el7 as the target instead of your local system)

```rpmbuild -bs --target=el7 ~/rpmbuild/SPECS/protobuf.spec```

This will build the new src rpm in "~/rpmbuild/SRPMS/protobuf-3.6.1-4.el8.src.rpm" (annoyingly still named after the host system, but just change it)
This file should then be moved to the same path as this readme file.

