INVENTORY_MONITOR_BRANCH=v0.4.1
INVENTORY_MONITOR_URI=https://gitlab.com/apollo-lhc/software/apollosm-inventory-monitor.git
INVENTORY_MONITOR_NAME=apollosm-inventory-monitor

%/opt/${INVENTORY_MONITOR_NAME}: OPT_PATH=$*/opt/

%/opt/${INVENTORY_MONITOR_NAME}: %/opt/
	@echo "============================================================"
	@echo "Adding Inventory monitoring"
	cd ${OPT_PATH} && \
		git clone --recursive --branch ${INVENTORY_MONITOR_BRANCH} ${INVENTORY_MONITOR_URI} ${INVENTORY_MONITOR_NAME}
