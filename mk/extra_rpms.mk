#Set OS variable
extra_rpms_alma8_% : OS=alma8
extra_rpms_centos7_% : OS=centos7

#set CPU variable
%xc7z035 : CPU=armv7hl
%xc7z045 : CPU=armv7hl
%xczu7ev : CPU=aarch64

#set qemu version
%xc7z035 : QEMU=qemu-arm-static
%xc7z045 : QEMU=qemu-arm-static
%xczu7ev : QEMU=qemu-aarch64-static

include mk/QEMU.mk

extra_rpms_%:
	@echo "============================================================"
	@echo "Applying extra rpms for ${OS}:${CPU}"
	#the - is to ignore the return code of this line
	mount -o bind /dev ./image/$*/dev/
	-test -d ./config/${OS}/rpms/*/*/${CPU}/ && sudo mkdir -p ./image/$*/tmp/rpms 
	#these copies are done blindly so name collisions will go unnoticed.   CPU specific has a higher priority than the no-arch packages
#	-test -d ./config/${OS}/rpms/*/*/${CPU}/ && sudo cp -r ./config/${OS}/rpms/*/*/noarch/* ./image/$*/tmp/rpms/
	-test -d ./config/${OS}/rpms/*/*/${CPU}/ && sudo cp -r ./config/${OS}/rpms/*/*/${CPU}/* ./image/$*/tmp/rpms/
	#The --ignorearch is there to deal with the confusion about armv7hl and armv7l.
	#This comes from the zynq being labeled as armv7l, but since it does have a hard floating point unit, the packages have armv7hl
	#TODO:  Some kind of test that this is only used in this situation and not aarch64 or another future platform.
	#I hope someone reads this before this is a problem.
	-test -d ./config/${OS}/rpms/*/*/${CPU}/ && chroot ./image/$*/ ${QEMU_PATH}/${QEMU} /usr/bin/rpm  -i -v --ignorearch /tmp/rpms/*.rpm
	-test -d ./config/${OS}/rpms/*/*/${CPU}/ && sudo rm -rf ./image/$*/tmp/rpms
	umount ./image/$*/dev/
