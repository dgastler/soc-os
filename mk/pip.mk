PIP_PACKAGE_FILE=${SCRIPTS_PATH}/../${OS}-rootfs/pip-packages.txt

pip_install_%_xc7z035 : QEMU=qemu-arm-static
pip_install_%_xc7z045 : QEMU=qemu-arm-static
pip_install_%_xczu7ev : QEMU=qemu-aarch64-static

#OS build
pip_install_alma8_% : OS=alma8
pip_install_centos7_% : OS=centos7


ENV_SET=LANG=en_US.UTF-8 LC_ALL=en_US.UTF-8 LC_CTYPE=en_US.UTF-8

pip_install_%:
	sudo cp /etc/resolv.conf ./image/$*/etc/
	sudo cp ${PIP_PACKAGE_FILE} ./image/$*/tmp/pip-packages.txt
	sudo mount --bind /dev/ ./image/$*/dev/
	${ENV_SET} chroot ./image/$*/ ${QEMU} /usr/bin/python3 -m pip install --upgrade pip
	${ENV_SET} chroot ./image/$*/ ${QEMU} /usr/bin/python3 -m pip install setuptools_rust wheel
	${ENV_SET} chroot ./image/$*/ ${QEMU} /usr/bin/python3 -m pip install --force-reinstall importlib_metadata packaging click==7.1.2 click_didyoumean pytest==4.6.9
	${ENV_SET} chroot ./image/$*/ ${QEMU} /usr/bin/python3 -m pip install -r /tmp/pip-packages.txt
	sudo rm ./image/$*/etc/resolv.conf
	sudo umount ./image/$*/dev/


